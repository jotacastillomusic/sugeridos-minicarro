/* eslint-disable no-console */
import React from "react";
import { useCssHandles } from 'vtex.css-handles'
import { useQuery } from 'react-apollo'
import { useState } from 'react'
import { useEffect } from 'react'
import Slider from "react-slick";
import { OrderFormProvider, useOrderForm } from 'vtex.order-manager/OrderForm'
import { Helmet } from 'vtex.render-runtime'
import { Dropdown } from 'vtex.styleguide'
import { useRuntime } from 'vtex.render-runtime'
import { useMutation } from 'react-apollo'
import MUTATIONS_ADD from './query/addToCart.gql'

import QUERY_ORDER from './query/orderId.gql'

const CSS_HANDLES = [
  'containerSugeridosMinicarro',
  'sugeridosBody',
  'sugeridosTitulo',
  'sugeridosTexto',
  'sugeridosTextop',
  'containerSlider',
  'contenedorProd',
  'contenedorProducto',
  'imagen',
  'imagen_img',
  'contenido',
  'nombre',
  'precio',
  'addToCart',
  'contenedorVariants',
  'selectorTallas'
]

let ultimaCateArr = "";
let ultimaCate = "";

const currency = function (number) {
  return new Intl.NumberFormat('es-CL', { style: 'currency', currency: 'CLP', minimumFractionDigits: 0 }).format(number);
};

const SugeridosMinicarro = (props) => {
  const [productosFiltrados, setProductosFiltrados] = useState([]);
  const { orderForm, setOrderForm } = useOrderForm()
  const [itemID, setItemID] = useState(0);
  const [state = { value:"" }, setState] = useState(0);

  const { account } = useRuntime()

  if (orderForm.items[orderForm.items.length - 1].productCategoryIds) {
    ultimaCateArr = orderForm.items[orderForm.items.length - 1].productCategoryIds.split("/");
    ultimaCate = ultimaCateArr[1];
  }

  const [lastCate, setLastCate] = useState(ultimaCate);
  useEffect(() => {
    fetch(`/api/catalog_system/pub/products/search?fq=C:${lastCate}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      }
    })
      .then(r => r.json())
      .then(function (response) {
        let arrayAux = []
        for (const products of response) {

          let options = []
          for(let i=0; i<products.items.length; i++){
            let obj = {value: products.items[i].itemId, label: products.items[i].name}
            options.push(obj)
          }
          arrayAux.push({
            sku: products.productId,
            precioReal: products.items[0].sellers[0].commertialOffer.ListPrice,
            nombre: products.productName,
            imagen: products.items[0].images[0].imageUrl,
            precio: products.items[0].sellers[0].commertialOffer.Price,
            link: products.link,
            items: products.items,
            opciones: options,
            disponible: products.items[0].sellers[0].commertialOffer.AvailableQuantity,
            desc: Math.trunc(((products.items[0].sellers[0].commertialOffer.ListPrice - products.items[0].sellers[0].commertialOffer.Price) * 100) / products.items[0].sellers[0].commertialOffer.ListPrice)
          });
        }
        setProductosFiltrados(arrayAux)
        console.log(arrayAux)
      })
  }, []);

  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  const handleResize = () => {
    setWidth(window.innerWidth);
  };

  let settings = {};
  const count = useCssHandles(CSS_HANDLES)
  if (width > 990) {
    settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      accessibility: true,
      className: count.sliderImage
    };
  } else if (width <= 990 && width > 640) {
    settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      accessibility: true,
      className: count.sliderImage
    };
  } else if (width <= 640) {
    settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      accessibility: true,
      className: count.sliderImage
    };
  }

  const [addToCar, { data, loading, error }] = useMutation(MUTATIONS_ADD)
  const addToCart = (sellerId) => {

    if(account=="casamarilla"){
      sellerId="1";
    }
    let orderId = orderForm.id;
    let cuerpo = { quantity: 1, seller: sellerId, id: parseInt(itemID) }

    let promise = addToCar({ variables: { idcart: orderId, item: cuerpo } })
    promise.then(function (response) {
      console.log(response)
      window.location.reload();
    })

  }

  function selectorTalla(itemId){
    console.log("talla elegida: "+itemId.value);
    setState({value: itemId.value})
    setItemID(itemId.value);
  }

  return (
    <div className={`${count.containerSugeridosMinicarro}`}>
      <Helmet>
        <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
      </Helmet>
      <div className={`${count.sugeridosBody}`}>
        <div className={`${count.sugeridosTitulo}`}>
          <span className={`${count.sugeridosTexto}`}><p className={`${count.sugeridosTextop}`}>Te podría interesar</p></span>
        </div>
        {
          productosFiltrados ? (
            <div className={`${count.containerSlider}`}>
              <Slider {...settings}>
                {
                  productosFiltrados.map((prod) => (
                    <div className={`${count.contenedorProd}`}>
                      <div className={`${count.contenedorProducto}`} style={{ cursor: "none" }} >
                        <a className={`${count.imagen}`} href={prod.link}>
                          <img className={`${count.imagen_img}`} src={prod.items[0].images[0].imageUrl} />
                        </a>
                        <div className={`${count.contenido}`} id={prod.items[0].itemId} >
                          <a className={`${count.nombre}`} href={prod.link} >
                            <p className={`${count.nombre}`} >{prod.nombre}</p>
                          </a>
                          {prod.opciones.length > 1
                            ? 
                            <div className={`${count.contenedorVariants}`}>
                            <Dropdown
                              placeholder="Selecciona"
                              options={prod.opciones}
                              value={state.value}
                              onChange={(_, v) => selectorTalla({ value: v })}
                              className={`${count.selectorTallas}`}
                            />
                          </div>
                            : <div></div>
                          }
                          <p className={`${count.precio}`} >{currency(prod.items[0].sellers[0].commertialOffer.ListPrice)}</p>
                          <button style={{ cursor: "pointer" }} className={`${count.addToCart}`} onClick={() => addToCart(prod.items[0].sellers[0].sellerId)}>+</button>
                        </div>
                      </div>
                    </div>
                  ))
                }
              </Slider>
            </div>
          ) : (
            <h1>hola mundo!</h1>
          )
        }
      </div>
    </div>
  );
}
export default SugeridosMinicarro